package com.santa.models;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "users")
public class User implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    private String email;
    private String password;

    @Enumerated(value = EnumType.STRING)
    private Role role;

    public static final String USER_ = "USER";
    public static final String ADMIN_ = "ADMIN";

    enum Role implements GrantedAuthority {
        USER(USER_), ADMIN(ADMIN_);


        private final String authority;

        Role(String authority) {
            this.authority = authority;
        }

        @Override
        public String getAuthority() {
            return authority;
        }
    }
}
