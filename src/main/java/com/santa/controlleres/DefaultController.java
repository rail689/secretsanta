package com.santa.controlleres;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DefaultController {


    @RequestMapping("/")
    public String index() {
        return "Welcome to Fight Club!<a href=\"/secret\">secret</a>";
    }

    @RequestMapping("/secret")
    public String secret() {
        return "The first rule of Fight Club is: You do not talk about Fight Club";
    }
}
