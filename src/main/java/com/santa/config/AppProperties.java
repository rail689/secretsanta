package com.santa.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "application")
public class AppProperties {

    public static final int DEFAULT_PAGE_SIZE = 10;

    private Db db;
    private Integer pageSize;


    public int pageSize() {
        return pageSize == null ? DEFAULT_PAGE_SIZE : pageSize;
    }

    @Data
    public static class Db {
        private String url;
        private String user;
        private String pass;
    }
}
