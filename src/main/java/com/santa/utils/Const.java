package com.santa.utils;

import java.util.Locale;

public class Const {

    public static final Locale DEFAULT_LOCALE = new Locale("ru", "ru");

    private Const(){}

}
